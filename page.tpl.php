<?php
?>
  <div id="page">
  <div id="wrapper">
  
    <div id="content">
      <div class="element-invisible"><a id="main-content"></a></div>
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>
      <?php if ($page['help']): ?>
        <div id="help">
          <?php print render($page['help']); ?>
        </div>
      <?php endif; ?>

<div id="hode">
<h4><a href="<?php print render($front_page); ?>"><?php print render($site_name); ?></a></h4>
<span><?php print render($site_slogan); ?></span>
</div>
      <div id="pagina">
        <?php print render($page['content']); ?>
      </div>
  </div>

      <div id="sidebar-1" class="sidebar"><ul>
        <?php print render($page['sidebar_1']); ?></ul>
      </div>
      <div id="sidebar-2" class="sidebar"><ul>
        <?php print render($page['sidebar_2']); ?></ul>
      </div>
      <div id="sidebar-3" class="sidebar"><ul>
        <?php print render($page['sidebar_3']); ?></ul>
      </div>
    </div>

  </div>
